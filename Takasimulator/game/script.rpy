﻿#personagens

define takanori = Character("Takanori")
define e = Character("Pessoa misteriosa")

define ragna = Character("Ragna")
image ragna11 = "KG110090.kg.png"
 
#fundos
image bgPark = ("bgpark.jpg")
               
image cafe = ("cafe.jpg")

# o jogo começa aqui 

label start:
    
    play music "03.eye-water.mp3"
 
    scene bgPark


   
    "Meu despertador toca mais uma vez e eu custo a me levantar, uma gota de suor escorre pelo meu rosto enquanto me encaminho até a cozinha para beber algo gelado."

    "O calor só tem aumentado, onde estão as chuvas de Dezembro? - pensei comigo"

    "Eu me chamo Takanori, e apesar de não me considerar otaku gosto bastante de animes e mangás."

    "Olhando para o relógio percebo que já são quase meio dia"

    takanori"Droga! Não fiz nada o dia todo. Porque fiquei assistindo animes a noite inteira?"

    "Me encaminho para o quarto e pegando a toalha me dirijo até o chuveiro."

    "Terminado o banho, escolho uma roupa e decido dar uma caminhada no parque."

    takanori"Acho que esta fica legal! Me vesti e caminhei em direção ao parque"

    "..."

    takanori"Nossa! O parque tem muita gente hoje, ele realmente fica bem cheio no fim de semana."

    takanori"Eu havia esquecido que hoje era sábado, vou procurar um lugar para sentar, não da pra caminhar por ai com tantas pessoas"

    "Uma pessoa misteriosa se aproxima."
    show ragna11 at center with dissolve
    e"Olá bela dama! Gostaria de comprar um chip da Tim?"
    "Eu o encarei por um instante e nada disse"

    e"Como você está?"

    takanori"Eu to bem, mas quem é você?"

    e"Eu sou o Ragna. Não se lembra de mim?"

    takanori"Ah! lembro sim, a gente estudou junto não foi?"

    ragna"Isso! Quer beber alguma coisa e conversar um pouco?"

    takanori"Só bora."

    takanori"Só vamos, tem um café aqui perto."
    "..."
    scene cafe
    "Chegando no café..."

menu : 
    takanori"O que devo pedir?"

    "café." : 
        "Eu bebo o café, e é bom até a última gota."

    "chá." : 
        $  drank_tea  =  True

        "Eu bebo o chá, tentando não falar de animes como eu sempre faço."



label  after_menu :

    "Depois de tomar minha bebida, eu tento puxa assunto"    

   
    return
